﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demos.DemosHelpers
{
    public enum SQLConditionRelationships
    {
        AND,
        OR
    }

    public enum AppActionCollectionAlterations
    {
        ADD,
        REMOVE
    }
}
