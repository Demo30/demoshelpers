﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Packaging;

namespace Demos.DemosHelpers
{
    public class PackagingTool
    {
        public static void PackageByteChunks(string destinationFullPath, Dictionary<string, byte[]> parts)
        {
            byte[] data = PackagingTool.PackageByteChunks(parts);

            using (FileStream fs = new FileStream(destinationFullPath, FileMode.CreateNew))
            {
                fs.Write(data, 0, data.Length);
            }
        }

        #warning assings xml mimetype to all parts regardless
        public static byte[] PackageByteChunks(Dictionary<string, byte[]> parts)
        {
            byte[] buffer = null;

            using (MemoryStream ms = new MemoryStream())
            using (Package package = Package.Open(ms, FileMode.CreateNew))
            {
                foreach(string key in parts.Keys)
                {
                    byte[] curChunk = parts[key];

                    Uri partUri = PackUriHelper.CreatePartUri(new Uri(key, UriKind.Relative));

                    PackagePart part = package.CreatePart(partUri, System.Net.Mime.MediaTypeNames.Text.Xml);

                    using (Stream st = part.GetStream())
                    {
                        st.Write(curChunk, 0, curChunk.Length);
                    }
                }

                package.Flush();
                buffer = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(buffer, 0, buffer.Length);
            }

            return buffer;
        }

        public static Package LoadPackage(string path, FileStream stream)
        {
            Package pack;

            pack = Package.Open(stream);

            return pack;
        }

        public static Package LoadPackage(byte[] data, MemoryStream stream)
        {
            stream.Write(data, 0, data.Length);

            Package pack;
            pack = Package.Open(stream);

            return pack;
        }
    }
}