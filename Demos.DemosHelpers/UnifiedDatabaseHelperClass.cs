﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Demos.DemosHelpers
{
    public class UnifiedDatabaseHelperClass
    {
        public static List<object[]> GetResultsList(string query, IDbConnection connection)
        {
            IDataReader reader = GetReader(query, connection);

            if (reader != null)
            {
                List<object[]> listOfRows = new List<object[]>();
                object[] rowOfData = new object[reader.FieldCount];

                for (int x = 0; x < reader.FieldCount; x++) rowOfData[x] = reader.GetName(x);

                listOfRows.Add(rowOfData);

                while (reader.Read())
                {
                    rowOfData = new object[reader.FieldCount];
                    reader.GetValues(rowOfData);
                    listOfRows.Add(rowOfData);
                }

                return listOfRows;
            }
            else return null;
        }

        public static DataTable GetResultsDataTable(string query, IDbConnection oracleConnection)
        {
            IDataReader reader = GetReader(query, oracleConnection);

            if (reader != null)
            {

                DataTable table = new DataTable();
                table.Load(reader);

                return table;
            }
            else
                return null;
        }

        public static int ExecuteNonQuery(string sql, IDbConnection connection, Dictionary<string, string> queryParams = null)
        {
            int affectedRows = -1;

            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (queryParams != null)
                {
                    foreach(string curParamKey in queryParams.Keys)
                    {
                        IDbDataParameter param = cmd.CreateParameter();
                        param.ParameterName = $"{curParamKey}";
                        param.Value = queryParams[curParamKey];
                        cmd.Parameters.Add(param);
                    }
                }
                affectedRows = cmd.ExecuteNonQuery();
            }
            return affectedRows;
        }

        public static IDataReader GetReader(string query, IDbConnection connection)
        {
            IDataReader reader = null;

            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = query;
                reader = cmd.ExecuteReader();
            }

            return reader;
        }
    }
}
