﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demos.DemosHelpers
{
    public interface IIdentifiable : IIdentity
    {
        IIdentity GetIdentity();
    }
}