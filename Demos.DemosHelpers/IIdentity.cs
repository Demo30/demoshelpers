﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demos.DemosHelpers
{
    public interface IIdentity
    {
        bool IsIdentical(object identity);
    }
}
