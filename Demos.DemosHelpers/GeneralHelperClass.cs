﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO.Packaging;
using System.Xml;
using System.IO;

namespace Demos.DemosHelpers
{
    public static class GeneralHelperClass
    {
        public enum SortingMethods
        {
            MergeSort
        }

        public delegate bool ComparisonMethod(object objectA, object objectB);

        #region Miscellaneous methods

        public static string GetXMLInPlaintext(XmlDocument doc)
        {
            XmlWriterSettings sett = new XmlWriterSettings();
            sett.Encoding = Encoding.UTF8;
            sett.Indent = true;

            using (StringWriter stringWriter = new Utf8StringWriter())
            using (XmlWriter xmlWriter = XmlWriter.Create(stringWriter, sett))
            {
                doc.Save(xmlWriter);

                xmlWriter.Flush();
                return stringWriter.GetStringBuilder().ToString();
            }
        }

        public static string NumToRomanNumbering(int number)
        {
            // from stack overflow: https://stackoverflow.com/questions/7040289
            // should think about this further, don't like the oorange exception...
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + GeneralHelperClass.NumToRomanNumbering(number - 1000);
            if (number >= 900) return "CM" + GeneralHelperClass.NumToRomanNumbering(number - 900);
            if (number >= 500) return "D" + GeneralHelperClass.NumToRomanNumbering(number - 500);
            if (number >= 400) return "CD" + GeneralHelperClass.NumToRomanNumbering(number - 400);
            if (number >= 100) return "C" + GeneralHelperClass.NumToRomanNumbering(number - 100);
            if (number >= 90) return "XC" + GeneralHelperClass.NumToRomanNumbering(number - 90);
            if (number >= 50) return "L" + GeneralHelperClass.NumToRomanNumbering(number - 50);
            if (number >= 40) return "XL" + GeneralHelperClass.NumToRomanNumbering(number - 40);
            if (number >= 10) return "X" + GeneralHelperClass.NumToRomanNumbering(number - 10);
            if (number >= 9) return "IX" + GeneralHelperClass.NumToRomanNumbering(number - 9);
            if (number >= 5) return "V" + GeneralHelperClass.NumToRomanNumbering(number - 5);
            if (number >= 4) return "IV" + GeneralHelperClass.NumToRomanNumbering(number - 4);
            if (number >= 1) return "I" + GeneralHelperClass.NumToRomanNumbering(number - 1);
            throw new ArgumentOutOfRangeException();
        }

#warning WIP - wrong >> AA, AB, AC not ZA, ZB, ZC....
        public static string NumToLatinNumbering(int number)
        {
            string[] latins = new string[]
            {
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
            };

            if (number < 1) { return string.Empty; }
            else if ((number) > latins.Length) { return latins[latins.Length - 1] + NumToLatinNumbering(number - latins.Length); }
            else { return latins[number - 1]; }
        }

        public static int NumberOfPlacesInNumberingSystem(int setLength, int decimalInteger)
        {
            int index = 0;
            int maxPerIndex = 0;

            while (maxPerIndex <= decimalInteger)
            {
                maxPerIndex = maxPerIndex + ((int)Math.Pow(setLength, index) * setLength);
                index++;
            }

            return index;
        }

#warning wrong...
        public static int[] GetIndexesPerNumberingSystem(int setLength, int decimalNumber)
        {
            List<int> indexes = new List<int>();

            if (decimalNumber == 0)
            {
                return new int[] { 0 };
            }

            int numOfPlaces = GeneralHelperClass.NumberOfPlacesInNumberingSystem(setLength, decimalNumber);

            int tempNum = decimalNumber + 1;

            for (int i = numOfPlaces; i >= 1; i--)
            {
                int indexToAdd = -1;

                for (int x = (setLength - 1); x >= 0; x--)
                {
                    int power = i - 1;
                    int max = (int)Math.Pow(setLength, power) * x;

                    if (max >= tempNum)
                    {
                        continue;
                    }
                    else
                    {
                        indexToAdd = x;
                        tempNum = tempNum - max;
                        break;
                    }
                }

                if (indexToAdd == -1)
                {
                    throw new InvalidOperationException();
                }

                indexes.Add(indexToAdd);
            }

            return indexes.ToArray();
        }

        public static XmlDocument AppendImport(this XmlDocument document, XmlNode appendImportNode, bool deep)
        {
            XmlNode clonedNode = document.ImportNode(appendImportNode, deep);
            document.AppendChild(clonedNode);

            return document;
        }

        public static ImageSource GetImageSourceFromString(string FilePath)
        {
            Uri auxUri = new Uri("pack://application:,,," + FilePath);
            BitmapFrame auxBitFrame = BitmapFrame.Create(auxUri);
            ImageSource result = auxBitFrame;

            return result;
        }

        /// <summary>Copies data from a source stream to a target stream.</summary>
        public static void CopyStream(Stream source, Stream target)
        {
            // from MSDN
            const int bufSize = 0x1000;
            byte[] buf = new byte[bufSize];
            int bytesRead = 0;
            while ((bytesRead = source.Read(buf, 0, bufSize)) > 0)
            {
                target.Write(buf, 0, bytesRead);
            }
        }

        /// <summary>
        /// In case the path exists on the PATH, return actual full path of the file
        /// </summary>
        public static string GetFullPath(string fileName)
        {
            /* From Stack Overflow: https://stackoverflow.com/questions/3855956*/

            if (File.Exists(fileName))
            {
                return Path.GetFullPath(fileName);
            }

            string values = Environment.GetEnvironmentVariable("PATH");
            foreach (var path in values.Split(';'))
            {
                string fullPath = Path.Combine(path, fileName);
                if (File.Exists(fullPath))
                {
                    return fullPath;
                }
            }
            return null;
        }

        public static void OpenFolderAndSelectFile(string[] filePaths)
        {
            for (int i = 0; i < filePaths.Length; i++)
            {
                if (File.Exists(filePaths[i]) == false)
                {
                    throw new ArgumentException($"Following file was not found: {filePaths[i]}");
                }
            }

            ShowSelectedInExplorer.FilesOrFolders(filePaths);
        }

        #endregion

        #region Collections-related methods

        public static T[] AddToArray<T>(this T[] array, out T[] newArray, object objectToAdd)
        {
            newArray = new T[array.Length + 1];
            array.CopyTo(newArray, 0);
            newArray.SetValue(objectToAdd, newArray.Length - 1);
            return newArray;
        }

        // Should think about implementing a more efficient version
        public static T[] RemoveFromArray<T>(this T[] array, out T[] newArray, T object2Remove, ComparisonMethod customComparisonMethod = null)
        {
            newArray = array;
            if (customComparisonMethod == null)
                customComparisonMethod = (x, y) => x.Equals(y);

            for (int tracker = 0; tracker < newArray.Length; tracker++)
            {
                T currentItem = newArray[tracker];
                if (customComparisonMethod(currentItem, object2Remove))
                {
                    newArray.RemoveFromArrayAtIndex(out newArray, tracker);
                    tracker--;
                }
            }

            return newArray;
        }

        // Should think about implementing a more efficient version
        public static T[] RemoveFromArrayAtIndex<T>(this T[] array, out T[] newArray, int indexToRemove)
        {
            newArray = new T[0];
            for (int tracker = 0; tracker < array.Length; tracker++)
            {
                if (tracker == indexToRemove)
                    continue;
                newArray.AddToArray<T>(out newArray, array[tracker]);
            }

            return newArray;
        }

        public static int[] GetIndexesOfObjectsWithinArray<T>(T[] arrayOfObjects, T[] objectsPresentInTheArray, ComparisonMethod customComparisonMethod = null)
        {
            int[] assignedIndexes = new int[0];
            if (arrayOfObjects != null)
            {
                for (int tracker = 0; tracker < objectsPresentInTheArray.Length; tracker++)
                {
                    T currentInspectedObject = objectsPresentInTheArray[tracker];
                    int currentIndex = GeneralHelperClass.GetIndexOfObjectWithinArray<T>(arrayOfObjects, currentInspectedObject, customComparisonMethod);

                    if (currentIndex == -1)
                        throw new Exception("Internal failure. Index of one of the objects could not be retrieved.");

                    assignedIndexes.AddToArray(out assignedIndexes, currentIndex);
                }
            }

            return assignedIndexes;
        }

        /// <summary>
        /// Gets the index of the object present in the provided array of objects.
        /// Custom ComparisonMethod() can be used to check equality between both objects.
        /// </summary>
        /// <returns>Index of the object or -1 on failure.</returns>
        public static int GetIndexOfObjectWithinArray<T>(T[] arrayOfObjects, T objectPresentInTheArrayOfObjects, ComparisonMethod customComparisonMethod = null)
        {
            if (customComparisonMethod == null)
                customComparisonMethod = (x, y) => x.Equals(y);

            int index;

            for (int tracker = 0; tracker < arrayOfObjects.Length; tracker++)
            {
                if (customComparisonMethod(arrayOfObjects[tracker], objectPresentInTheArrayOfObjects))
                {
                    index = tracker;
                    return index;
                }
            }

            index = -1;
            return index;
        }

        public static string ArrayToString(this Array collection, string separator)
        {
            return ArrayToString(collection, separator, new Tuple<string, string>(String.Empty, String.Empty));
        }

        public static string ArrayToString(this Array collection, string separator, Tuple<string, string> wrappers)
        {
            StringBuilder sb = new StringBuilder();
            foreach (object currentRecord in collection)
            {
                if (sb.Length != 0)
                    sb.Append(separator);
                sb.Append(String.Concat(
                    wrappers.Item1,
                    currentRecord.ToString(),
                    wrappers.Item2));
            }
            return sb.ToString();
        }

        public static List<T> ToList<T>(this T[] array, out List<T> list)
        {
            list = new List<T>();
            list.AddRange(array);
            return list;
        }


        /// <summary>
        /// In case the index is higher than the length of the array. The item is inserted at the end of array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="newArray"></param>
        /// <param name="item"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T[] InsertAtIndex<T>(this T[] array, out T[] newArray, T item, int index)
        {
            if (index > array.Length - 1)
                index = array.Length;
            else if (index < 0)
                throw new ArgumentException("Invalid index value. Index needs to be positive.");

            newArray = new T[array.Length + 1];

            if (index > 0)
            {
                Array.Copy(array, 0, newArray, 0, index);
            }
            newArray[index] = item;
            if (index < array.Length)
            {
                Array.Copy(array, index, newArray, index + 1, (array.Length - index));
            }

            return newArray;
        }

        /// <summary>
        /// Can be meaningully applied only to collections sorted by the same comparison method as one passed to this method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="searchedItem"></param>
        /// <param name="isFirstLargerThanSecond"></param>
        /// <param name="stepMethod"></param>
        /// <param name="lowerBound"></param>
        /// <param name="upperBound"></param>
        /// <returns>Returns -1 if the item is not found</returns>
        public static int BinarySearch<T>(T[] array, T searchedItem, Func<T, T, bool> isFirstLargerThanSecond, Action stepMethod)
        {
            if (array == null || searchedItem == null)
                throw new ArgumentNullException();

            if (isFirstLargerThanSecond == null)
                throw new ArgumentNullException(String.Format("A {0} comparison method needs to be specified.", nameof(isFirstLargerThanSecond)));

            if (stepMethod == null)
                stepMethod = () => { return; };

            int resultIndex = -1;

            resultIndex = BinarySearchRecursive(array, searchedItem, isFirstLargerThanSecond, stepMethod, 0, array.Length - 1);

            return resultIndex;
        }

        private static int BinarySearchRecursive<T>(T[] array, T searchedItem, Func<T, T, bool> isFirstLargerThanSecond, Action stepMethod, int lowerBound, int upperBound)
        {
            int resultIndex = -1;

            int middle = ((upperBound - lowerBound) / 2) + lowerBound;

            stepMethod();

            // Middle value is larger, need to go left
            if (isFirstLargerThanSecond(array[middle], searchedItem) && lowerBound < middle)
            {
                resultIndex = BinarySearchRecursive(array, searchedItem, isFirstLargerThanSecond, stepMethod, lowerBound, middle - 1);
            }
            // Middle value is lower, need to go right
            else if (isFirstLargerThanSecond(searchedItem, array[middle]) && upperBound > middle)
            {
                resultIndex = BinarySearchRecursive(array, searchedItem, isFirstLargerThanSecond, stepMethod, middle + 1, upperBound);
            }
            // assuming that they have to equal then... which I should not..do...?
            else if (!isFirstLargerThanSecond(array[middle], searchedItem) && !isFirstLargerThanSecond(searchedItem, array[middle]))
            {
                resultIndex = middle;
            }

            return resultIndex;
        }

        public static void SortArray<T>(T[] array, out T[] sortedArray, Func<T, T, bool> isFirstLargerThanSecond, SortingMethods method, Action stepAction = null)
        {
            sortedArray = null;
            switch (method)
            {
                case SortingMethods.MergeSort:
                    MergeSort(array, out sortedArray, isFirstLargerThanSecond, stepAction);
                    break;
                default:
                    throw new NotImplementedException(String.Format("An implementation is missing for {0} sorting method", nameof(method)));
            }
        }

        private static T[] MergeSort<T>(T[] array, out T[] sortedArray, Func<T, T, bool> isFirstLargerThanSecond, Action stepAction = null)
        {
            if (array == null)
                throw new ArgumentNullException("You need to supply an array.");

            if (isFirstLargerThanSecond == null)
                throw new ArgumentNullException(String.Format("A {0} comparison method needs to be specified.", nameof(isFirstLargerThanSecond)));

            if (stepAction != null)
                stepAction();

            if (array.Length < 2)
            {
                sortedArray = array;
                return sortedArray;
            }

            int middlePoint = (int)(array.Length / 2);

            T[] left = array.Take(middlePoint).ToArray();
            T[] right = array.Skip(middlePoint).Take(array.Length - middlePoint).ToArray();

            left = MergeSort(left, out left, isFirstLargerThanSecond, stepAction);
            right = MergeSort(right, out right, isFirstLargerThanSecond, stepAction);

            sortedArray = MergeSorted(left, right, isFirstLargerThanSecond);

            return sortedArray;
        }

        /// <summary>
        /// Supplied comparison method needs to specify behaviour upon null encounter.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="firstSortedArray"></param>
        /// <param name="secondSortedArray"></param>
        /// <param name="isFirstLargerThanSecond"></param>
        /// <returns></returns>
        public static T[] MergeSorted<T>(T[] firstSortedArray, T[] secondSortedArray, Func<T, T, bool> isFirstLargerThanSecond)
        {
            if (firstSortedArray == null || secondSortedArray == null || isFirstLargerThanSecond == null)
                throw new ArgumentNullException();

            T[] result = new T[firstSortedArray.Length + secondSortedArray.Length];

            for (int tracker = 0, leftTracker = 0, rightTracker = 0; tracker < result.Length; tracker++)
            {
                bool leftSideFinished = leftTracker >= firstSortedArray.Length;
                bool rightSideFinished = rightTracker >= secondSortedArray.Length;

                if (leftSideFinished == true && rightSideFinished == false)
                {
                    result[tracker] = secondSortedArray[rightTracker];
                    rightTracker++;
                    continue;
                }
                else if (leftSideFinished == false && rightSideFinished == true)
                {
                    result[tracker] = firstSortedArray[leftTracker];
                    leftTracker++;
                    continue;
                }

                if (isFirstLargerThanSecond(firstSortedArray[leftTracker], secondSortedArray[rightTracker]) == false)
                {
                    result[tracker] = firstSortedArray[leftTracker];
                    leftTracker++;
                }
                else if (isFirstLargerThanSecond(firstSortedArray[leftTracker], secondSortedArray[rightTracker]) == true)
                {
                    result[tracker] = secondSortedArray[rightTracker];
                    rightTracker++;
                }
            }

            return result;
        }

        public static T[] AddToSorted<T>(T[] sortedArray, out T[] newSortedArray, T itemToAdd, Func<T, T, bool> isFirstLargerThanSecond)
        {
            return AddToSorted(sortedArray, out newSortedArray, new T[] { itemToAdd }, isFirstLargerThanSecond);
        }

        public static T[] AddToSorted<T>(T[] sortedArray, out T[] newSortedArray, T[] itemsToAdd, Func<T, T, bool> isFirstLargerThanSecond)
        {
            newSortedArray = new T[sortedArray.Length + itemsToAdd.Length];

            for (int tracker = 0; tracker < newSortedArray.Length; tracker++)
            {
                if (tracker < itemsToAdd.Length)
                    newSortedArray[tracker] = itemsToAdd[tracker];
                else
                    newSortedArray[tracker] = sortedArray[tracker - itemsToAdd.Length];
            }

            SortArray(newSortedArray, out newSortedArray, isFirstLargerThanSecond, SortingMethods.MergeSort);

            return newSortedArray;
        }
        #endregion

        #region Numerical methods

        public enum ToInt32ConversionTypes
        {
            IntOrException,
            IntOrZero,
            IntOrMinusOne
        }

        public static int ToInt32(this object object4Conversion, ToInt32ConversionTypes conversionType)
        {
            int result = 0;
            if (object4Conversion == null || !(Int32.TryParse(object4Conversion.ToString(), out result)))
            {
                switch (conversionType)
                {
                    case ToInt32ConversionTypes.IntOrException:
                        throw new Exception("Provided input object couldn't be converted to Int32.");
                    case ToInt32ConversionTypes.IntOrZero:
                        break;
                    case ToInt32ConversionTypes.IntOrMinusOne:
                        result = -1;
                        break;
                }

                return result;
            }
            else
                return result;
        }

        /// <summary>
        /// New lines are accepted. Returns exception with desired message on failed check.
        /// </summary>
        /// <param name="inputObject"></param>
        /// <param name="exceptionMesage"></param>
        public static void ConsistsOfIntegers(object inputObject, string exceptionMesage)
        {
            if (!ConsistsOfIntegers(inputObject))
                throw new Exception(exceptionMesage);
        }

        /// <summary>
        /// New lines are accepted.
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns>bool</returns>
        public static bool ConsistsOfIntegers(object inputObject)
        {
            string input = inputObject.ToString();
            string[] splittedRows = input.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            foreach (string currentLine in splittedRows)
            {
                int integerTest;
                if (!Int32.TryParse(currentLine, out integerTest))
                    return false;
            }

            return true;
        }

        public static float Clamp(this float number, float lowerBound, float upperBound)
        {
            if (number >= lowerBound && number <= upperBound)
                return number;
            else if (number < lowerBound)
                return lowerBound;
            else
                return upperBound;
        }

        public static bool EqualEpsilon(float a, float b, float epsilon)
        {
            if (Math.Abs(a - b) < epsilon)
                return true;
            else
                return false;
        }

        public static double GetMedian(double[] values)
        {
            double[] tempValues = new double[values.Length];
            Array.Copy(values, tempValues, values.Length);

            if (tempValues == null || tempValues.Length == 0)
            {
                throw new InvalidInputException();
            }
            else if (tempValues.Length == 1)
            {
                return tempValues[0];
            }

            Array.Sort(tempValues);

            if (tempValues.Length % 2 != 0)
            {
                int mid = (int)Math.Floor((double)(tempValues.Length / 2));
                return tempValues[mid];
            }
            else
            {
                int mid = tempValues.Length / 2;
                double median = (tempValues[mid] + tempValues[mid - 1]) / 2;
                return median;
            }
        }
        #endregion

        #region String-related methods

        public static string[] SplitNewLineDelimetedString(this string original)
        {
            string[] result = original.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.None);

            return result;
        }

        #endregion

        #region  File manipulation methods

        /// <summary>
        /// Prompts user for filepath, saves the file and opens the file if desired.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="forcedExtension"></param>
        /// <param name="showFile"></param>
        public static void SaveFile(byte[] data, string forcedExtension = null, bool showFile = true)
        {
            System.Windows.Forms.SaveFileDialog dia = new System.Windows.Forms.SaveFileDialog() { };
            System.Windows.Forms.DialogResult diaResult = dia.ShowDialog();

            if (diaResult == System.Windows.Forms.DialogResult.OK)
            {
                string path = GeneralHelperClass.ForceExtension(dia.FileName, forcedExtension);

                FileInfo fi = new FileInfo(path);

                if (fi.Exists)
                {
                    File.Delete(fi.FullName);
                }

                using (FileStream fs = new FileStream(fi.FullName, FileMode.Create))
                {
                    fs.Write(data, 0, data.Length);
                }

                if (showFile)
                {
                    // last action - opening the result
                    GeneralHelperClass.ShowFileCascade(fi);
                }
            }
        }

        /// <summary>
        /// Attempts to open the file. On fail, attempts to open folder and select the files.
        /// </summary>
        /// <param name="file"></param>
        public static bool ShowFileCascade(FileInfo file)
        {
            bool lastActionSuccess = false;
            try
            {
                System.Diagnostics.Process.Start(file.FullName);
                lastActionSuccess = true;
            }
            catch { }

            if (!lastActionSuccess)
            {
                try
                {
                    ShowSelectedInExplorer.FileOrFolder(file.FullName);
                    lastActionSuccess = true;
                }
                catch { }
            }

            if (!lastActionSuccess)
            {
                try
                {
                    ShowSelectedInExplorer.FileOrFolder(file.DirectoryName);
                    lastActionSuccess = true;
                }
                catch { }
            }

            if (!lastActionSuccess)
            {
                try
                {
                    System.Diagnostics.Process.Start(file.DirectoryName);
                    lastActionSuccess = true;
                }
                catch { }
            }

            return lastActionSuccess;
        }

        public static string ForceExtension(string fullPath, string extension)
        {
            if (String.IsNullOrEmpty(extension))
            {
                return fullPath;
            }

            string newName = fullPath;
            if (newName.Last() == '.')
            {
                newName = newName.Substring(0, newName.Length - 1);
            }

            if (extension.First() != '.')
            {
                extension = String.Concat(".", extension);
            }

            if ((newName.Length < 4) || (newName.Substring(newName.Length - 4, 4) != extension))
            {
                newName = String.Concat(newName, extension);
            }

            return newName;
        }

        public static void CopyDirectoriesWithContent(DirectoryInfo source, DirectoryInfo target)
        {
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    target.CreateSubdirectory(diSourceSubDir.Name);
                CopyDirectoriesWithContent(diSourceSubDir, nextTargetSubDir);
            }
        }

        #endregion
    }
}
