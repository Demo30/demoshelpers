﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Demos.DemosHelpers
{
    public class DemosHelpersMeta
    {
        public static Versioning Version
        {
            get
            {
                string versionString = Assembly.GetExecutingAssembly()
                    .GetName()
                    .Version
                    .ToString();
                Versioning versioning = new Versioning();
                versioning.LoadVersion(versionString);
                return versioning;
            }
        }
    }
}
