﻿// Demos.DemosHelpers.Versioning
using Demos.DemosHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demos.DemosHelpers
{
    public class Versioning
    {
        private int[] _version = new int[1];

        public int[] Version
        {
            get
            {
                return _version;
            }
            set
            {
                if (value == null || value.Length == 0)
                {
                    throw new ArgumentNullException();
                }
                _version = value;
            }
        }

        public int[] LoadVersion(string version, string separator = ".")
        {
            string[] array = version.Split(separator.ToArray());
            List<int> versionLevels = new List<int>();
            string[] array2 = array;
            for (int i = 0; i < array2.Length; i++)
            {
                int curVer = array2[i].ToInt32(GeneralHelperClass.ToInt32ConversionTypes.IntOrException);
                versionLevels.Add(curVer);
            }
            Version = versionLevels.ToArray();
            return versionLevels.ToArray();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Version.Length; i++)
            {
                int ver = Version[i];
                if (i > 0)
                {
                    sb.Append(".");
                }
                sb.Append(ver.ToString());
            }
            return sb.ToString();
        }

        public static bool operator ==(Versioning v1, Versioning v2)
        {
            if (((object)v1 == null && (object)v2 != null) || ((object)v2 == null && (object)v1 != null))
            {
                return false;
            }
            if ((object)v1 == null && (object)v2 == null)
            {
                return true;
            }
            if (v1.Version == null || v1.Version.Length == 0 || v2.Version == null || v2.Version.Length == 0)
            {
                throw new ArgumentNullException();
            }
            int iterations = (v1.Version.Length > v2.Version.Length) ? v1.Version.Length : v2.Version.Length;
            for (int i = 0; i < iterations; i++)
            {
                int num = (v1.Version.Length - 1 >= i) ? v1.Version[i] : 0;
                int v2CurVer = (v2.Version.Length - 1 >= i) ? v2.Version[i] : 0;
                if (num != v2CurVer)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool operator !=(Versioning v1, Versioning v2)
        {
            return !(v1 == v2);
        }

        public static bool operator >(Versioning v1, Versioning v2)
        {
            if (v1 == null || v2 == null)
            {
                throw new ArgumentNullException();
            }
            int iterations = (v1.Version.Length > v2.Version.Length) ? v1.Version.Length : v2.Version.Length;
            for (int i = 0; i < iterations; i++)
            {
                int num = (v1.Version.Length - 1 >= i) ? v1.Version[i] : 0;
                int v2CurVer = (v2.Version.Length - 1 >= i) ? v2.Version[i] : 0;
                if (num > v2CurVer)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool operator <(Versioning v1, Versioning v2)
        {
            if (v1 == null || v2 == null)
            {
                throw new ArgumentNullException();
            }
            if (!(v1 == v2))
            {
                return !(v1 > v2);
            }
            return false;
        }

        public static bool operator >=(Versioning v1, Versioning v2)
        {
            if (v1 == null || v2 == null)
            {
                throw new ArgumentNullException();
            }
            return !(v1 < v2);
        }

        public static bool operator <=(Versioning v1, Versioning v2)
        {
            if (v1 == null || v2 == null)
            {
                throw new ArgumentNullException();
            }
            return !(v1 > v2);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Versioning))
            {
                return false;
            }
            return this == (Versioning)obj;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            for (int i = 0; i < Version.Length; i++)
            {
                int multiplier = i + 1;
                hash += Version[i] * multiplier;
            }
            return hash;
        }
    }
}