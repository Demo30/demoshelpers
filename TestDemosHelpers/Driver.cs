﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demos.DemosHelpers;
using System.Threading;

namespace Demos.Tests.DemosHelpers
{
    public class Driver
    {
        static void Main(string[] args)
        {
            bool repeat = true;

            while (repeat == true)
            {
                TestPackagingToolPackage();

                Console.WriteLine(Environment.NewLine + "Repeat (Y/N)?");
                string prompt = Console.ReadLine();
                if (prompt != "Y")
                    repeat = false;
            }
        }

        public static void TestPackagingToolPackage()
        {
            Dictionary<string, byte[]> content = new Dictionary<string, byte[]>();

            content.Add("test001", new byte[] { 0x0d, 0x52, 0xAC, 0xF1 });

            byte[] inBytes = PackagingTool.PackageByteChunks(content);

            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < inBytes.Length; i++)
            {
                sb.Append(inBytes[i].ToString());
            }

            Console.WriteLine(sb.ToString());
        }

        public static void TestInsertAtIndex()
        {
            string[] array = new string[] {
                "nultý", "první", "druhý", "třetí", "čtvrtý"
            };

            int[] testIndexes = new int[] { -3, 0, 2, 4, 5, 20 };

            string testword = "pokus";
            string nullWord = "null ref";

            foreach (int testIndex in testIndexes)
            {
                Console.WriteLine(String.Format("Vlož {1} na pozici {0}:", testIndex, testword));
                string[] testArray = new string[array.Length];
                array.CopyTo(testArray, 0);
                try
                {
                    testArray.InsertAtIndex(out testArray, testword, testIndex);

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message + Environment.NewLine);
                    continue;
                }
                for (int tracker = 0; tracker < testArray.Length; tracker++)
                {
                    string item = nullWord;
                    if (testArray[tracker] != null)
                        item = testArray[tracker].ToString();

                    Console.WriteLine(String.Format("Index {0}: {1}", tracker, item));
                }

                int resultIndex = testIndex;
                if (resultIndex > testArray.Length - 1)
                    resultIndex = testArray.Length - 1;
                else if (resultIndex < 0)
                    resultIndex = 0;


                if (testArray[resultIndex] != testword || testArray.Contains(null))
                    Console.WriteLine("Pozor: asi chyba" + Environment.NewLine);
                else
                    Console.WriteLine("Seems OK!" + Environment.NewLine);
            }

            // testing Efficiency
            int timerResolution = 500;

            int arraySize = (int)Math.Pow(10,8);
            string[] effArr = new string[arraySize];

            Console.WriteLine(String.Format("Test efficiency on array of size {0}:", arraySize));

            int numMs = 0;
            Timer timer = new Timer(x => numMs++, null, 0, timerResolution);

            numMs = 0;
            int index = new Random().Next(0, effArr.Length);

            Console.WriteLine("Inserting at " + index);
            timer.Change(0, timerResolution);

            effArr.InsertAtIndex(out effArr, "9999", index);

            timer.Change(0, -1);
            Console.WriteLine(String.Format("The process took about {0}ms ({1}).", numMs* timerResolution, DateTime.Now));
        }

        public static void TestBinarySearch()
        {
            Console.WriteLine("Testing binary search.");
            Console.WriteLine("Preparing data...");
            int arrayLength = (int)Math.Pow(10, 8);
            int[] sortedArray = new int[arrayLength];
            int differ = 100;

            for (int tracker = 0; tracker < sortedArray.Length; tracker++)
            {
                sortedArray[tracker] = differ + tracker;
            }

            Console.WriteLine("Data prepared." + Environment.NewLine);

            int[] testSearchedItems = new int[] { -800, 0, differ, 850, arrayLength - 1 + differ, arrayLength + 5000 };

            for (int tracker = 0; tracker < testSearchedItems.Length; tracker++)
            {
                Console.WriteLine(String.Format("Running binary search #{1} ({0}):", DateTime.Now, tracker + 1));
                int searchedInt = testSearchedItems[tracker];
                int iterationsCouter = 0;
                int resultIndex = GeneralHelperClass.BinarySearch(sortedArray, searchedInt, (x, y) => x > y, new Action(() => iterationsCouter++));

                bool isCorrect = false;
                if (resultIndex == -1 || sortedArray[resultIndex] != searchedInt) isCorrect = false; else isCorrect = true;

                if ((searchedInt < differ || searchedInt > arrayLength - 1 + differ) && resultIndex == -1)
                    isCorrect = true;

                Console.WriteLine(String.Format("Binary search finished."));
                Console.WriteLine(String.Format("It went through {0} iterations and ended at {1}.", iterationsCouter, DateTime.Now));
                Console.WriteLine(String.Format("The resulting index is {0}. Which is: {1}.", resultIndex, isCorrect));

                Console.WriteLine(Environment.NewLine);
            }
        }

        public static void TestMergeSort()
        {
            Console.WriteLine("Preparing data...");

            int[][] testUnsortedArrays = new int[2][];
            testUnsortedArrays[0] = new int[] {
                -9, 0, 68, 54, 70, 1, 2, 3, 55, 54
            };
            int secondArrayLength = (int)Math.Pow(10, 4);
            testUnsortedArrays[1] = new int[secondArrayLength];
            Random panNahoda = new Random();
            for (int tracker = 0; tracker < secondArrayLength; tracker++)
            {
                testUnsortedArrays[1][tracker] = panNahoda.Next(-500, 10000);
            }

            for (int tracker = 0; tracker < testUnsortedArrays.Length; tracker++)
            {
                Console.WriteLine(String.Format("Sorting array #{0}: {1}", tracker + 1, testUnsortedArrays[tracker].ArrayToString(", ")));

                int recursions = 0;

                DateTime beginning = DateTime.Now;

                int[] result = null;
                GeneralHelperClass.SortArray(testUnsortedArrays[tracker], out result, (x, y) => x > y, GeneralHelperClass.SortingMethods.MergeSort, new Action(() => recursions++));

                TimeSpan duration = DateTime.Now - beginning;

                if (result != null)
                {
                    Console.WriteLine("Result: " + result.ArrayToString(", "));
                    Console.WriteLine(String.Format("Took {0} calls.", recursions));
                    Console.WriteLine(String.Format("Took about {0}ms", duration.Milliseconds));
                }
                else
                    Console.WriteLine("Result is empty.");

                Console.WriteLine(Environment.NewLine + "//////////////////////" + Environment.NewLine);
            }
        }

        public static void TestAddToSorted()
        {
            Console.WriteLine("Preparing data...");

            int[] sortedArray = new int[]{
                -50, -3, 0, 12, 18, 150
            };
            int[] newItems = new int[] { 90, -45, 0, -90, 200};

            Console.WriteLine(String.Format("About to add unsorted values to a sorted array.{0}Values: {1}{0}Sorted array: {2}", Environment.NewLine, newItems.ArrayToString(", "), sortedArray.ArrayToString(", ")));

            GeneralHelperClass.AddToSorted(sortedArray, out sortedArray, newItems, (x, y) => x > y);

            Console.WriteLine(String.Format("Outcome: {0}", sortedArray.ArrayToString(", ")));

        }

        public static void TestGetMedian()
        {
            List<double[]> listOfValueLists = new List<double[]>()
            {
                new double [] { },
                new double [] { 5 },
                new double [] { 0, 1 },
                new double [] { 1, 2 },
                new double [] { 1, 2, 3, 4},
                new double [] { 0, 1, 2, 3, 4},
                new double [] { 1, 2, 3, 4, 5},
                new double [] { 0.3, 0.6, 0.12, 3, 3.5},
                new double [] { 0.3, 0.6, 0.12, 3, 3.5, 4.1},
                new double [] { 0.12, 0.3, 0.6, 0.8, 0.9, 50 }
            };

            StringBuilder output = new StringBuilder();

            foreach(double[] listOfValues in listOfValueLists)
            {
                output.Append(Environment.NewLine + "For values: ");
                output.Append(string.Join("; ", listOfValues));
                try
                {
                    double median = GeneralHelperClass.GetMedian(listOfValues);
                    output.Append(" => " + "Calculated median: " + median);
                }
                catch (Exception)
                {
                    output.Append("Error");
                }
            }

            output.Append(Environment.NewLine);
            Console.Write(output.ToString());
        }
    }
}
